//
//  MLScheduler.swift
//


/**
 * 调用放调用下边的方式；
 * 实现放需要ext 这个类和方法去处理
 *
 * 命名规则，MLScheduler[实现放Pod名称]
 * 方法名字格式 ml_ 加后边的功能
 */
@objcMembers
@objc(MLSchedulerKit)
open class MLSchedulerKit: NSObject {
    
    /**
     实例：
     获取当前用户的登录信息
     @return 用户信息
     @param 用户ID
     */
    public dynamic static func ml_getLoginInfo(userId: String) -> [AnyHashable: Any]? {
        return nil
    }
}
