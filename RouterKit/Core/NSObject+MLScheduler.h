//
//  NSObject+MLScheduler.h
//

#import <Foundation/Foundation.h>
#import "MLSchedulerCore.h"

#define ml_scheduler_getClass(className) ([[MLSchedulerCore shared] getClassWithName: className])

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (MLScheduler)

- (nullable id) ml_execMethod:(nullable SEL) selector;
- (nullable id) ml_execMethod:(nullable SEL) selector params:(nullable NSArray *) params;

@end

NS_ASSUME_NONNULL_END
