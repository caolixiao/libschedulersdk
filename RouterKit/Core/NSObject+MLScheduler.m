//
//  NSObject+MLScheduler.m
//

#import "NSObject+MLScheduler.h"

@implementation NSObject (MLScheduler)

- (nullable id) ml_execMethod:(nullable SEL) selector {
    return [[MLSchedulerCore shared] ml_execInstanceMethod: selector inTarget: self params: nil];
}

- (nullable id) ml_execMethod:(nullable SEL) selector params:(nullable NSArray *)params {
    return [[MLSchedulerCore shared] ml_execInstanceMethod: selector inTarget: self params: params];
}

- (nullable id) ml_execMethodWithUrl:(nonnull NSURL *) url {
    return [[MLSchedulerCore shared] ml_execMethodWithUrl: url];
}

@end
