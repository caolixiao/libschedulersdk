//
//  MLScheduler.m
//

#import "MLScheduler.h"
#import "MLSchedulerCore.h"
#import "NSObject+MLScheduler.h"
#import <LibBaseSDK/UIView+MLCustom.h>

@implementation MLScheduler

+ (nullable id) open:(NSString *) path {
    UIViewController *vc = [[MLSchedulerCore shared] ml_execMethodWithUrl: [NSURL URLWithString: path]];
    if ([vc isKindOfClass: [UIViewController class]]) {
        UIViewController *vc1 = UIApplication.sharedApplication.keyWindow.currentViewController;
        UINavigationController *nav = vc1.navigationController;
        if (nav) [nav pushViewController: vc animated: true];
        else [vc1 presentViewController: vc animated: true completion:nil];
    }
    return vc;
}

+ (NSNumber*) op {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    // TODO: -
#pragma clang diagnostic pop
}

@end
