//
//  MLRouterUrlSchemes.swift
//

/**
 * 注册可跳转的URL
 */

// MARK: - 可以跳转的API
public enum MLRouterUrlSchemes {
    
    /// 授权pod
    case auth(Auth)
    
    // 授权跳转的地方
    public enum Auth {
        /// 登录界面
        case login(Int64)
        
        /// 注册界面
        case regist(Int64)

        /// 重置密码
        case rest
    }
    
    // todo: -
}

extension MLRouterUrlSchemes {
    public var host: String {
        return "router://MLRouterUrlExt"
    }
}


// 具体实现的跳转URL
extension MLRouterUrlSchemes {
    public var rawValue: String {
        var value = ""
        switch self {
        case .auth(.login(let id)):
            value = "\(host)/aaaWithId:?id=\(id)"
            break
        case .auth(.regist(let id)):
            value = "\(host)/aaaWithId:?id=\(id)"
            break
        case .auth(.rest):
            value = "\(host)/aaa"
            break
        }
        return value
    }
}
