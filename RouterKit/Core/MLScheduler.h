//
//  MLScheduler.h
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MLScheduler : NSObject

+ (nullable id) open:(NSString *) url;

@end

NS_ASSUME_NONNULL_END
