//
//  MLScheduler.swift
//

import UIKit
import LibBaseSDK


/**
 * 调用方式
 * MLRouterUrl.open(MLRouterUrlSchemes.detail(id: 123))
 * MLRouterUrl.open(path: "router://MLRouterUrlExt/aaaWithId:?id=60")
 */
public class MLRouterUrl: NSObject {
    
    @discardableResult
    public static func open(_ urlSchemes: MLRouterUrlSchemes) -> Any? {
        return MLRouterUrl.open(path: urlSchemes.rawValue)
    }
    
    @discardableResult
    public static func open(path: String) -> Any? {
        if let url = URL(string: path),
            let result = MLSchedulerCore.shared().ml_execMethod(with: url) as? UIViewController {
            guard let vc = UIApplication.shared.keyWindow?.currentViewController() else { return result }
            
            if let nav = vc.navigationController {
                nav.pushViewController(result, animated: true)
            } else {
                vc.present(result, animated: true, completion: nil)
            }
            return result
        }
        return nil
    }
}
