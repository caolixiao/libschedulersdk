Pod::Spec.new do |s|
    
  s.name         = "LibSchedulerSDK"
  s.version      = "1.0.0"
  s.summary      = "LibSchedulerSDK SDK."
  s.description  = "LibSchedulerSDK SDK for iOS."
  
  s.homepage     = "http://www.toby.ml"
  s.license      = "MIT"
  s.author       = { "caolixiao" => "caolixiao@yeah.net" }
  
  s.platform     = :ios, "9"
  s.ios.deployment_target = "9"
  
  s.source       = {
      :git => "https://caolixiao@bitbucket.org/caolixiao/libschedulersdk.git",
      :tag => "v#{s.version}"
  }
  
  s.requires_arc = true
  s.static_framework = true
  s.swift_version = '5.0'
  
  s.pod_target_xcconfig = {
      "GCC_PREPROCESSOR_DEFINITIONS" => "$(inherited) LibSchedulerSDK_NAME=#{s.name} LibSchedulerSDK_VERSION=#{s.version}"
  }
  
  s.default_subspecs = ["Base", "SchedulerKit", "RouterKit"]
  
  ### 基础设置
  s.subspec "Base" do |ss|
      ss.source_files = ["Foundation/**/*.{h,m}", "UIKit/**/*.{h,m}"]
      ss.public_header_files = [ "Foundation/**/*.h", "UIKit/**/*.h"]
      ss.frameworks = ["Foundation", "UIKit"]
    
      ss.dependency 'LibBaseSDK'
      ss.dependency 'LibUidesignSDK'
  end

  ### 平行SDK调度库 【下层库】
  s.subspec "SchedulerKit" do |ss|
    ss.source_files =
    "SchedulerKit/*.swift"
    
    ss.dependency 'LibSchedulerSDK/Base'
  end
  
  ### RouterKit 【上层库】
  s.subspec "RouterKit" do |ss|
    ss.public_header_files =
    "RouterKit/**/*.h"
    
    ss.source_files =
    "RouterKit/*.{h,m,swift}",
    "RouterKit/**/*.{h,m,swift}"
    
    ss.dependency 'LibSchedulerSDK/Base'
  end

end
